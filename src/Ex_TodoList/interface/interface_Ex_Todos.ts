export interface InterfaceTodo {
  id: number;
  text: string;
  isCompleted: boolean;
}

export interface InterfaceTodoListComponent {
  todos: InterfaceTodo[];
}

export interface InterfaceTodoItemComponent {
  item: InterfaceTodo;
}

export interface InterfaceTodoFormComponent {
  handleAddTodo: (item: InterfaceTodo) => void;
}
