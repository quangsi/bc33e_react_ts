import React from "react";
import { InterfaceTodoListComponent } from "../interface/interface_Ex_Todos";
import TodoItem from "../TodoItem/TodoItem";

export default function TodoList({ todos }: InterfaceTodoListComponent) {
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Text</th>
            <th>IsCompleted</th>
          </tr>
        </thead>
        <tbody>
          {todos.map((item, index) => {
            return <TodoItem item={item} key={index} />;
          })}
        </tbody>
      </table>
    </div>
  );
}

// import React from 'react'

// export default function TodoList({}) {
//   return (
//     <div>{list}</div>
//   )
// }
