import React, { useState } from "react";
import { InterfaceTodo } from "./interface/interface_Ex_Todos";
import TodoForm from "./TodoForm/TodoForm";
import TodoList from "./TodoList/TodoList";

type Props = {};

export default function Ex_TodoList({}: Props) {
  const [todos, setTodos] = useState<InterfaceTodo[]>([
    {
      id: 1,
      text: "Làm dự án cuối khoá",
      isCompleted: true,
    },
    {
      id: 2,
      text: "Lau nhà",
      isCompleted: false,
    },
  ]);
  const handleAddTodo = (newtodo: InterfaceTodo) => {
    let newTodos = [...todos, newtodo];
    setTodos(newTodos);
  };
  return (
    <div className="container py-5">
      <TodoForm handleAddTodo={handleAddTodo} />
      <TodoList todos={todos} />
    </div>
  );
}

// tsrfc
