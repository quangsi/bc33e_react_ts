import React from "react";
import UserInfor from "./UserInfor";
export interface InterfaceUser {
  name: string;
  age: number;
}

export default function DemoProps() {
  let user: InterfaceUser = {
    name: "alice",
    age: 2,
  };
  return (
    <div>
      <UserInfor data={user} />
    </div>
  );
}

// tsrfc
